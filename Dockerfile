#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

FROM python:3.9-slim-bullseye
LABEL project=ironic-credentials

ENV VIRTUAL_ENV=/virtual_env
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY ironic_credentials /src/ironic_credentials
COPY setup.py setup.cfg MANIFEST.in /src/
WORKDIR /src
RUN pip install .
USER 999
CMD ["ironic-credentials"]
