#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -euo pipefail

for var in NEXUS_KANOD_USER NEXUS_REGISTRY REPO_URL IMAGE_SYNC_DIR VERSION
do
    if [ -z "${!var}" ]; then
        echo "${var} must be defined"
        exit
    fi
done

export CONTAINER=ironic-credentials

echo "${NEXUS_KANOD_PASSWORD}" | docker login -u "${NEXUS_KANOD_USER}" --password-stdin "${NEXUS_REGISTRY}"

docker build --build-arg "http_proxy=${http_proxy:-}" --build-arg "https_proxy=${https_proxy:-}" --build-arg "no_proxy=${no_proxy:-}"  . -t "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

docker push "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
    docker image prune -a --force --filter 'label=project=ironic-credentials'
fi
