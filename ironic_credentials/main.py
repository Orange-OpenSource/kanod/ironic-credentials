#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import argparse
import base64
import bcrypt
import logging
import os
import uuid

from kubernetes import client  # type: ignore
from kubernetes import config  # type: ignore

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO)


def b64(s):
    return base64.b64encode(s.encode('utf-8')).decode('ascii')


def generate_password():
    return str(uuid.uuid4()).replace('-', '')[0:16]


def htpasswd(username, passwd):
    salt = bcrypt.gensalt(rounds=5)
    crypted = bcrypt.hashpw(passwd.encode('utf-8'), salt).decode('ascii')
    return f"{username}:{crypted}"


def main():
    parser = argparse.ArgumentParser(prog='ironic-credentials')
    parser.add_argument('--inspector', action=argparse.BooleanOptionalAction)
    args = parser.parse_args()
    namespace = os.environ.get('TARGET_NAMESPACE', 'default')
    config.load_incluster_config()
    corev1 = client.CoreV1Api()

    def make_secret(name, data, credentials=False):
        log.info(f'Create secret {name}')
        translated_data = {
            key: b64(value) for (key, value) in data.items()
        }
        body = {
            'apiVersion': 'v1',
            'data': translated_data,
            'kind': 'Secret',
            'metadata': {
                'name': name,
                'namespace': namespace
            },
            'type': ('kubernetes.io/basic-auth' if credentials else 'Opaque')
        }
        corev1.create_namespaced_secret(namespace, body)

    ironic_user = 'ironic'
    ironic_password = generate_password()

    # inspector_user = 'inspector'
    # inspector_password = generate_password()
    inspector_user = ironic_user
    inspector_password = ironic_password

    mariadb_password = generate_password()

    try:
        print('ironic-htpasswd')
        make_secret(
            'ironic-htpasswd',
            {'HTTP_BASIC_HTPASSWD': htpasswd(ironic_user, ironic_password)})
    except Exception:
        log.exception('cm ironic-htpasswd creation failed')
    try:
        if args.inspector:
            make_secret(
                'ironic-inspector-htpasswd',
                {'HTTP_BASIC_HTPASSWD':
                    htpasswd(inspector_user, inspector_password)})
    except Exception:
        log.exception('cm ironic-inspector-htpasswd creation failed')
    try:
        ironic_auth_config = (
            '[ironic]\nauth_type=http_basic\n'
            f'username={ironic_user}\npassword={ironic_password}\n')
        make_secret('ironic-auth-config', {'auth-config': ironic_auth_config})
    except Exception:
        log.exception('secret creation ironic-auth-config failed')
    try:
        if args.inspector:
            inspector_auth_config = (
                '[inspector]\nauth_type=http_basic\n'
                f'username={inspector_user}\npassword={inspector_password}\n')
            make_secret(
                'ironic-inspector-auth-config',
                {'auth-config': inspector_auth_config})
    except Exception:
        log.exception('secret creation ironic-inspector-auth-config failed')
    try:
        rpc_auth_config = (
            '[json_rpc]\nauth_type=http_basic\n'
            f'username={ironic_user}\npassword={ironic_password}\n'
            f'http_basic_username={ironic_user}\n'
            f'http_basic_password={ironic_password}\n')
        make_secret(
            'ironic-rpc-auth-config',
            {'auth-config': rpc_auth_config})
    except Exception:
        log.exception('secret creation ironic-rpc-auth-config failed')
    try:
        make_secret(
            'ironic-credentials',
            {"username": ironic_user, "password": ironic_password},
            credentials=True)
    except Exception:
        log.exception('secret creation ironic-credentials failed')
    try:
        if args.inspector:
            make_secret(
                'ironic-inspector-credentials',
                {"username": inspector_user, "password": inspector_password},
                credentials=True)
    except Exception:
        log.exception('secret creation ironic-inspector-credentials failed')

    try:
        make_secret(
            'mariadb-password',
            {"password": mariadb_password}
        )
    except Exception:
        log.exception('secret creation mariadb-password failed')
